# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=kturtle
pkgver=21.04.2
pkgrel=0
# armhf blocked by extra-cmake-modules
# s390x, mips64 and riscv64 blocked by polkit -> kio
arch="all !armhf !s390x !mips64 !riscv64"
url="http://edu.kde.org/kturtle"
pkgdesc="Educational Programming Environment"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcoreaddons-dev
	kcrash-dev
	kdoctools-dev
	ki18n-dev
	kio-dev
	knewstuff-dev
	ktextwidgets-dev
	qt5-qtbase-dev
	qt5-qtsvg-dev
	"
source="https://download.kde.org/stable/release-service/$pkgver/src/kturtle-$pkgver.tar.xz"
subpackages="$pkgname-doc $pkgname-lang"
options="!check" # No tests

build() {
	cmake -B build \
		-DCMAKE_BUILD_TYPE=None \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	cd build
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
c14f8de4bbefec14c8c5aa107141fc5b6ffeece503938ebb318623190752848cb2461dea80df996a840e380def3a3b70d86e82e592ace36d16c044c6762cda97  kturtle-21.04.2.tar.xz
"
